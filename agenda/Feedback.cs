﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace agenda
{
    public class Feedback
    {
        private Label msgLabel;

        public Feedback(Form formInstance)
        {
            String labelName = "msgLabel";

            var label = formInstance.Controls.Find(labelName, true).FirstOrDefault();
            if(null != label && label is Label)
            {
                this.msgLabel = (label as Label);
            }
        }

        public void set_message(String type, String message)
        {
            switch (type)
            {
                case "err":
                    set_err_display();
                    this.msgLabel.Text += message;
                    break;
                case "info":
                    set_info_display();
                    this.msgLabel.Text += message;
                    break;
                case "succ":
                    set_succ_display();
                    this.msgLabel.Text += message;
                    break;
                default:
                    this.msgLabel.Text = "";
                    break;
            }

        }

        public void reset_message()
        {
            this.msgLabel.Text = "";
        }

        private void set_err_display()
        {
            this.msgLabel.ForeColor = System.Drawing.Color.Red;
            this.msgLabel.Text = "Error:";
        }

        private void set_info_display()
        {
            this.msgLabel.ForeColor = System.Drawing.Color.Gray;
            this.msgLabel.Text = "Info:";
        }

        private void set_succ_display()
        {
            this.msgLabel.ForeColor = System.Drawing.Color.Green;
        }
    }
}
