﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace agenda
{
    public partial class Form1 : Form
    {
        public Feedback feedback;
        public Validator validator;
        public PersonDAO agenda;
        public MyJSON jsonHelper;

        public Form1()
        {
            InitializeComponent();

            feedback = new Feedback(this);
            validator = new Validator();
            agenda = new PersonDAO();
            jsonHelper = new MyJSON("agenda.json");

            //deserialize json file to list
            List<Person> tempList = jsonHelper.deserializeFileToList();
            if (tempList != null)
            {
                agenda.insertPersonList(tempList);

                refresh_dataGridView(agenda.getAll());
            }
        }

        private void refresh_dataGridView(List<Person> personList)
        {
            this.dataGridView1.DataSource = null;
            this.dataGridView1.DataSource = personList;

            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.ClearSelection();

            this.dataGridView1.Columns[0].Width = 150;
            this.dataGridView1.Columns[1].Width = 100;
            this.dataGridView1.Columns[2].Width = 150;

            this.dataGridView1.Refresh();
        }

        private void phoneTB_Validating(object sender, CancelEventArgs e)
        {
            if (phoneTB.Text.Length > 0)
            {
                if(validator.check_phone(phoneTB.Text))
                {
                    feedback.set_message("succ", "Phone number is valid!");
                }
                else
                {
                    feedback.set_message("err", "Invalid phone number!");
                }
            }

        }

        private void phoneTB_Enter(object sender, EventArgs e)
        {
            String  message  = "Accepted phone number formats: \n";
                    message += "+1 123 123 123 \n";
                    message += "+12 123 123 123 \n";
                    message += "+123 123 123 123 \n";
            feedback.set_message("info", message);
        }

        private void phoneTB_Leave(object sender, EventArgs e)
        {
            feedback.reset_message();
        }

        private void plusBtn_Click(object sender, EventArgs e)
        {
            //this.dataGridView1.Rows.Add("five", "six", "seven");
            feedback.reset_message();

            if (validator.isValidForm())
            {
                String name = nameTB.Text;
                String phone = phoneTB.Text;
                String email = emailTB.Text;

                Person pers = new Person(name, phone, email);

                if (!agenda.checkExisting(pers))
                {
                    agenda.insertPerson(pers);

                    refresh_dataGridView(agenda.getAll());
                    clearForm();
                    feedback.set_message("succ", "Insert was successfull!");
                }
                else
                {
                    feedback.set_message("err", "Identical record found!");
                }
            }
            else
            {
                feedback.set_message("err", "Check invalid fields!");
            }
        }

        private void emailTB_Validating(object sender, CancelEventArgs e)
        {
            if (emailTB.Text.Length > 0)
            {
                if (validator.check_email(emailTB.Text))
                {
                    feedback.set_message("succ", "Email is valid!");
                }
                else
                {
                    feedback.set_message("err", "Invalid email!");
                }
            }
        }

        private void emailTB_Leave(object sender, EventArgs e)
        {
            feedback.reset_message();
        }

        private void nameTB_Validating(object sender, CancelEventArgs e)
        {
            validator.check_name(nameTB.Text);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            String name = nameTB.Text;
            String phone = phoneTB.Text;
            String email = emailTB.Text;

            validator.setValidValues(name, phone, email);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            //serialize list into json file 
            if (agenda.getAll().Any())
            {
                jsonHelper.serializeListToFile(agenda.getAll());
            }
            else
            {
                jsonHelper.emptyFile();
            }
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            feedback.reset_message();

            if (this.dataGridView1.SelectedRows.Count > 0)
            {
                DataGridViewRow row = this.dataGridView1.SelectedRows[0];
                String name = row.Cells[0].Value.ToString();
                String phone = row.Cells[1].Value.ToString();
                String email = row.Cells[2].Value.ToString();

                Person pers = new Person(name, phone, email);

                nameTB.Text = name;
                phoneTB.Text = phone;
                emailTB.Text = email;

                agenda.setPersonBuffer(pers);

                feedback.set_message("info", "Selected row will be available for update");
            }
        }

        private void minusBtn_Click(object sender, EventArgs e)
        {
            feedback.reset_message();
            Person pers = agenda.getPersonBuffer();
            if (pers != null)
            {
                agenda.removePerson(pers);

                refresh_dataGridView(agenda.getAll());
                clearForm();
                feedback.set_message("succ", "Contact deleted!");
            }
        }

        private void okBtn_Click(object sender, EventArgs e)
        {
            String name = nameTB.Text;
            String phone = phoneTB.Text;
            String email = emailTB.Text;
            Person newPerson = new Person(name, phone, email);

            feedback.reset_message();

            if (validator.check_entire_form(name, phone, email))
            {
                if (!agenda.checkExisting(newPerson))
                {
                    agenda.updatePerson(agenda.getPersonBuffer(), newPerson);

                    refresh_dataGridView(agenda.getAll());
                    clearForm();
                    feedback.set_message("succ", "Update was successfull!");
                }
                else
                {
                    feedback.set_message("err", "Identical record found!");
                }
            }
            else
            {
                feedback.set_message("err", "Check invalid fields!");
            }
        }


        //========= Private functions ==========
        private void clearForm()
        {
            nameTB.Text = "";
            phoneTB.Text = "";
            emailTB.Text = "";
        }

        private void searchTB_TextChanged(object sender, EventArgs e)
        {
            String filter = searchTB.Text;
            
            refresh_dataGridView(agenda.searchNamesLike(filter));
            //refresh_dataGridView(agenda.search(filter));
        }

        private void searchTB_MouseEnter(object sender, EventArgs e)
        {
            feedback.reset_message();
            feedback.set_message("info", "Search by name");
        }

        private void searchTB_MouseLeave(object sender, EventArgs e)
        {
            feedback.reset_message();
        }
    }
}
