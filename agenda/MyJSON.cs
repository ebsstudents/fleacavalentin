﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;


namespace agenda
{
    public class MyJSON
    {
        private String json;
        private String file;

        public MyJSON(String file)
        {
            this.file = file;
        }

        public void emptyFile()
        {
            if (File.Exists(file))
            {
                File.Delete(file);

                var myFile = File.Create(file);
                myFile.Close();
            }
        }

        public void serializeListToFile(List<Person> persList)
        {
            if(!File.Exists(file))
            {
                var myFile = File.Create(file);
                myFile.Close();
            }

            this.json = JsonConvert.SerializeObject(persList);
            File.WriteAllText(file, json);

        }

        public List<Person> deserializeFileToList()
        {
            if (!File.Exists(file))
            {
                var myFile = File.Create(file);
                myFile.Close();
            }

            if (new FileInfo(file).Length != 0)
            {
                this.json = File.ReadAllText(file);
                var personList = JsonConvert.DeserializeObject<List<Person>>(json);

                return personList;
            }

            return null;
        }
    }
}
