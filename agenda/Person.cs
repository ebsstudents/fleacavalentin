﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace agenda
{
    public class Person
    {
        public String name { get; set;  }
        public String phone { get; set;  }
        public String email { get; set; }

        public Person(String name, String phone, String email)
        {
            this.phone = phone;
            this.email = email;
            this.name = name;
        }
    }
}
