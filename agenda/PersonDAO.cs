﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace agenda
{
    public class PersonDAO
    {
        private List<Person> agenda;
        private Person personBuffer;

        public PersonDAO()
        {
            agenda = new List<Person>();
        }

        public void setPersonBuffer(Person p)
        {
            this.personBuffer = p;
        }
        public Person getPersonBuffer()
        {
            return this.personBuffer;
        }


        public void insertPerson(Person pers)
        {
            agenda.Add(pers);
        }

        public void removePerson(Person pers)
        {
            //agenda.Remove(pers);
            int counter = 0;
            foreach (Person p in agenda)
            {
                if (p.name == pers.name && p.phone == pers.phone && p.email == pers.email)
                {
                    agenda.RemoveAt(counter);
                    return;
                }

                counter++;
            }
        }

        public List<Person> getAll()
        {
            return this.agenda;
        }

        public void insertPersonList(List<Person> persList)
        {
            agenda.AddRange(persList);
        }

        public void updatePerson(Person pers, Person newPers)
        {
            int counter = 0;
            foreach(Person p in agenda)
            {
                if(p.name == pers.name && p.phone == pers.phone && p.email == pers.email)
                {
                    agenda[counter] = newPers;
                    return;
                }

                counter++;
            }
        }

        public Boolean checkExisting(Person pers)
        {
            foreach (Person p in agenda)
            {
                if (p.name == pers.name && p.phone == pers.phone && p.email == pers.email)
                {
                    return true;
                }
            }

            return false;

        }

        public List<Person> searchNamesLike(String str)
        {
            List<Person> toReturn = new List<Person>();
            foreach(Person p in agenda)
            {
                if (p.name.Contains(str))
                {
                    toReturn.Add(p);
                }
            }

            return toReturn;
        }

        public List<Person> search(String str)
        {
            List<Person> toReturn = new List<Person>();
            foreach (Person p in agenda)
            {
                if (p.name.Contains(str) || p.phone.Contains(str) || p.email.Contains(str))
                {
                    toReturn.Add(p);
                }
            }

            return toReturn;
        }

    }
}
