﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace agenda
{
    public class Validator
    {
        public Boolean valid_phone;
        public Boolean valid_email;
        public Boolean valid_name;

        public Validator()
        {
            this.valid_phone = false;
            this.valid_email = false;
            this.valid_name = false;
        }

        public Boolean check_phone(String text)
        {
            Regex phoneNumpattern = new Regex(@"\+[0-9]{1,3}\s+[0-9]{3}\s+[0-9]{3}\s+[0-9]{3}");
            if (phoneNumpattern.IsMatch(text))
            {
                this.valid_phone = true;
            }
            else
            {
                this.valid_phone = false;
            }

            return this.valid_phone;
        }

        public Boolean check_email(String text)
        {
            try
            {
                var eMailValidator = new System.Net.Mail.MailAddress(text);
                this.valid_email = true;
            }
            catch (FormatException ex)
            {
                this.valid_email = false;
            }

            return this.valid_email;
        }

        public Boolean check_name(String text)
        {
            if(text.Length > 0)
                this.valid_name = true;
            else
                this.valid_name = false;

            return this.valid_name;
        }

        public Boolean isValidForm()
        {
            return this.valid_email && this.valid_phone && this.valid_name;
        }

        public Boolean check_entire_form(String name, String phone, String email)
        {
            return this.check_email(email) && this.check_name(name) && this.check_phone(phone);
        }

        public void setValidValues(String name, String phone, String email)
        {
            if (email.Length > 0)
                this.valid_email = this.check_email(email);
            else
                this.valid_email=false;

            if (phone.Length > 0)
                this.valid_phone = this.check_phone(phone);
            else
                this.valid_phone = true;

            if (name.Length > 0)
                this.valid_name = this.check_name(name);
            else
                this.valid_phone = false;   
        }
    }
}
